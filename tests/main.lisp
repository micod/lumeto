(defpackage lumeto/tests/main
  (:use :cl
        :lumeto
        :rove))
(in-package :lumeto/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :lumeto)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))

(deftest test-make-vec3
  (testing "creating vec3"
    (let* ((x 1.0)
           (y 2.0)
           (z 3.0)
           (vec (lumeto:make-vec3 :x x :y y :z z)))
      (ok (and (= x (lumeto:vec3-x vec))
               (= y (lumeto:vec3-y vec))
               (= z (lumeto:vec3-z vec)))))))

(deftest test-vec3+
  (testing "sum of vec3"
    (let* ((vector-1 (make-vec3 :x 1.0 :y 2.0 :z 3.0))
           (vector-2 (make-vec3 :x 2.0 :y 4.0 :z 6.0))
           (vector-3 (make-vec3 :x 3.0 :y 6.0 :z 9.0))
           (vector-4 (make-vec3 :x 6.0 :y 12.0 :z 18.0)))
      (ok (equalp (lumeto:vec3+) (make-vec3 :x 0.0 :y 0.0 :z 0.0)))
      (ok (equalp (lumeto:vec3+ vector-1) vector-1))
      (ok (equalp (lumeto:vec3+ vector-1 vector-2) vector-3))
      (ok (equalp (lumeto:vec3+ vector-1 vector-2 vector-3) vector-4)))))
