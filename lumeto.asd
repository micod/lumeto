(defsystem "lumeto"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "vec3")
                 (:file "ray" :depends-on ("vec3"))
                 (:file "main" :depends-on ("vec3" "ray")))))
  :description ""
  :in-order-to ((test-op (test-op "lumeto/tests"))))

(defsystem "lumeto/tests"
  :author ""
  :license ""
  :depends-on ("lumeto"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for lumeto"
  :perform (test-op (op c) (symbol-call :rove :run c)))
