(defpackage lumeto
  (:use :cl)
  (:export
   #:make-vec3
   #:vec3-x
   #:vec3-y
   #:vec3-z
   #:vec3+
   #:vec3+=
   #:vec3-
   #:vec3-=
   #:vec3*
   #:vec3*=
   #:vec3/
   #:vec3/=
   #:vec3-length
   #:vec3-length-squared
   #:vec3-dot
   #:vec3-cross
   #:vec3-unit-vector))
(in-package :lumeto)

(defstruct vec3
  (x 0.0 :type single-float)
  (y 0.0 :type single-float)
  (z 0.0 :type single-float))

(defun vec3+ (&rest vectors)
  (let ((result (make-vec3)))
    (dolist (v vectors)
      (incf (vec3-x result) (vec3-x v))
      (incf (vec3-y result) (vec3-y v))
      (incf (vec3-z result) (vec3-z v)))
    result))

(defun vec3+= (v1 v2)
  (incf (vec3-x v1) (vec3-x v2))
  (incf (vec3-y v1) (vec3-y v2))
  (incf (vec3-z v1) (vec3-z v2)))

(defun vec3- (vector &rest vectors)
  (if vectors
      (let ((x (vec3-x vector))
            (y (vec3-y vector))
            (z (vec3-z vector)))
        (dolist (v vectors)
          (decf x (vec3-x v))
          (decf y (vec3-y v))
          (decf z (vec3-z v)))
        (make-vec3 :x x :y y :z z))
      (vec3* -1.0 vector)))

(defun vec3-= (v1 v2)
  (decf (vec3-x v1) (vec3-x v2))
  (decf (vec3-y v1) (vec3-y v2))
  (decf (vec3-z v1) (vec3-z v2)))

(defgeneric vec3* (a b)
  (:documentation "Generic vector multiplication."))

(defmethod vec3* ((v vec3) (c number))
  (make-vec3 :x (* (vec3-x v) c)
             :y (* (vec3-y v) c)
             :z (* (vec3-z v) c)))

(defmethod vec3* ((c number) (v vec3))
  (vec3* v c))

(defmethod vec3* ((v1 vec3) (v2 vec3))
  (make-vec3 :x (* (vec3-x v1) (vec3-x v2))
             :y (* (vec3-y v1) (vec3-y v2))
             :z (* (vec3-z v1) (vec3-z v2))))

(defun vec3*= (v c)
  (setf (vec3-x v) (* (vec3-x v) c))
  (setf (vec3-y v) (* (vec3-y v) c))
  (setf (vec3-z v) (* (vec3-z v) c)))

(defun vec3/ (v c)
  (vec3* v (/ 1.0 c)))

(defun vec3/= (v c)
  (vec3*= v (/ 1.0 c)))

(defun vec3-length (v)
  (sqrt (vec3-length-squared v)))

(defun vec3-length-squared (v)
  (+ (expt (vec3-x v) 2) (expt (vec3-y v) 2) (expt (vec3-z v) 2)))

(defun vec3-dot (v1 v2)
  (+ (* (vec3-x v1) (vec3-x v2)) (* (vec3-y v1) (vec3-y v2)) (* (vec3-z v1) (vec3-z v2))))

(defun vec3-cross (v1 v2)
  (make-vec3 :x (- (* (vec3-y v1) (vec3-z v2)) (* (vec3-z v1) (vec3-y v2)))
             :y (- (* (vec3-z v1) (vec3-x v2)) (* (vec3-x v1) (vec3-z v2)))
             :z (- (* (vec3-x v1) (vec3-y v2)) (* (vec3-y v1) (vec3-x v2)))))

(defun vec3-unit-vector (v)
  (vec3/ v (vec3-length v)))
