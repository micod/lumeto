(defpackage lumeto
  (:use :cl)
  (:export
   #:render))
(in-package :lumeto)

(defun write-color (stream color)
  (format stream "~A ~A ~A~%"
          (truncate (* 255.999 (vec3-x color)))
          (truncate (* 255.999 (vec3-y color)))
          (truncate (* 255.999 (vec3-z color)))))

(defun hit-sphere (center radius r)
  (let* ((oc (vec3- (ray-origin r) center))
         (a (vec3-length-squared (ray-direction r)))
         (half-b (vec3-dot oc (ray-direction r)))
         (c (- (vec3-length-squared oc) (* radius radius)))
         (discriminant (- (* half-b half-b) (* a c))))
    (if (< discriminant 0)
        -1.0
        (/ (- (- half-b) (sqrt discriminant)) a))))

(defun ray-color (r)
  (let ((c (hit-sphere (make-vec3 :z -1.0) 0.5 r)))
    (when (> c 0.0)
      (let ((N (vec3-unit-vector (vec3- (ray-at r c) (make-vec3 :z -1.0)))))
    (return-from ray-color (vec3* 0.5 (vec3+ N (make-vec3 :x 1.0 :y 1.0 :z 1.0 )))))))

  (let* ((unit-direction (vec3-unit-vector (ray-direction r)))
         (a (* 0.5 (+ (vec3-y unit-direction) 1.0))))
    (vec3+ (vec3* (- 1.0 a) (make-vec3 :x 1.0 :y 1.0 :z 1.0)) (vec3* a (make-vec3 :x 0.5 :y 0.7 :z 1.0)))))

(defun render ()
  (let* ((aspect-ratio (/ 16.0 9.0))
         (image-width 400)
         (image-height (max (truncate (/ image-width aspect-ratio)) 1))
         (focal-length 1.0)
         (viewport-height 2.0)
         (viewport-width (* viewport-height (/ (coerce image-width 'float) image-height)))
         (camera-center (make-vec3))
         (viewport-u (make-vec3 :x viewport-width))
         (viewport-v (make-vec3 :y (- viewport-height)))
         (pixel-delta-u (vec3/ viewport-u image-width))
         (pixel-delta-v (vec3/ viewport-v image-height))
         (viewport-upper-left (vec3- camera-center (make-vec3 :z focal-length) (vec3/ viewport-u 2) (vec3/ viewport-v 2)))
         (pixel00-loc (vec3+ viewport-upper-left (vec3* 0.5 (vec3+ pixel-delta-u pixel-delta-v)))))
    (with-open-file (f #p"out.ppm" :direction :output :if-exists :overwrite :if-does-not-exist :create)
      (format f "P3~%~A ~A~%255~%" image-width image-height)
      (loop :for j :below image-height :do
        (format t "Scanlines remaining: ~A~%" (- image-height j))
        (loop :for i :below image-width :do
          (let* ((pixel-center (vec3+ pixel00-loc (vec3* i pixel-delta-u) (vec3* j pixel-delta-v)))
                 (ray-direction (vec3- pixel-center camera-center))
                 (r (make-ray :origin camera-center :direction ray-direction))
                 (pixel-color (ray-color r)))
            (write-color f pixel-color)))))))
