(defpackage lumeto
  (:use :cl)
  (:export
   #:make-ray
   #:ray-origin
   #:ray-direction
   #:ray-at))
(in-package :lumeto)

(defstruct ray
  (origin    (make-vec3) :type vec3)
  (direction (make-vec3) :type vec3))

(defun ray-at (ray c)
  (vec3+ (ray-origin ray) (vec3* c (ray-direction ray))))
