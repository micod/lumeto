* Lumeto
Pathtracer written in Common Lisp based on the book series [[https://raytracing.github.io/][Ray Tracing in One Weekend]].
Lumeto is an Esperanto word meaning /little light/, which should be enough for a little renderer.
